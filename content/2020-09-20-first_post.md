---
date: 2020-09-20
author: RGarcia
title: First Post!
category: blog
---

# Este bló

Diseñamos y construimos prendas de vestir y otras cosas textilosas por
que tenemos el mandato divino de Crear.


Además acudimos al llamado altruista de compartir con los demás, así
que compartimos lo que vamos aprendiendo y consideramos que podría ser
útil. 


Además compartimos planes digitalizados, que son bien interesantes de
hacer.


¡Gracias por leer! Comentarios: ya saben dónde encontrarnos.

:-)